# Modeler

2D modeler and simulator.


## Getting Started

### Dependencies

* All dependencies from [CHPACK.jl](https://gitlab.com/lcc-uff/Chpack.jl) and [hetool](https://gitlab.com/danilosb/hetoollibrary)
* [zmq for Python](https://zeromq.org/languages/python/)
* [zmq for Julia](https://github.com/JuliaInterop/ZMQ.jl)
* [npz for Julia](https://github.com/fhs/NPZ.jl)

### Executing program

* To run the simulator, navigate to '/simulator' and run the command below:
```
python3 simulator_optimal.py & julia ch2ep_stress_optimal_socket.jl &
```
* (Integration with the main app is still WIP)