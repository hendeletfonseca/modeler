#testFailureMatrix = [[True, False, True, False],[False, True, False, True]] * 2

def failureMatrixToSymbols(failure_matrix):
    fullSqr = '■'
    emptySqr = '□'

    b = []

    for i in failure_matrix:
        c = []
        for j in i:
            c.append(fullSqr) if (j == True or j =='■') else c.append(emptySqr)
        b.append(c)
    
    return b

def prettyPrintSymbols(symbols):
    for i in symbols:
        print(*i)

def prettyPrintFailureMatrix(failure_matrix):
    symbols = failureMatrixToSymbols(failure_matrix)
    prettyPrintSymbols(*symbols)

def rawFileToSymbols(rawFile):
    symbolDict = {}
    symbolDict[255] = '□'
    symbolDict[0] = '□'
    undefinedSymbol = '■'

    symbolList = []
    with open(rawFile, "br") as rawValue:
        for r in rawValue.read():
            if r in symbolDict:
                symbolList.append(symbolDict[r])
            else:
                symbolList.append(undefinedSymbol)
    
    return symbolList


if __name__ == "__main__":
    result = rawFileToSymbols("temp/circle1000x1000.raw")
    print(*result)
    # import numpy as np
    # failureMatrix = np.load("temp/failed.npy")
    
    # mySymbols = failureMatrixToSymbols(failureMatrix)
    # prettyPrintSymbols(mySymbols)
    #print("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    #prettyPrintSymbols(np.reshape(mySymbols, (10, 20)))
    #print("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    #prettyPrintSymbols(np.transpose(mySymbols))
    #print("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")