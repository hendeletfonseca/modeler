# TODO
#- Send 'quit' message to Julia if the user closes using the 'X' window button
#- Optimal Von Mises (need to implement maximum() for the Von Mises Criterion)
#- Integrate Julia into Python

# HOW TO USE
# 1. Create a folder for the input and put the '.tif' image inside
# 2. Generate JSON and edit the desired properties
# 3. Run simulation (default step (Δε) = 0.0005)

import json
import os
import sys
import shutil
import zmq
from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import (FigureCanvasQTAgg, NavigationToolbar2QT)
from matplotlib.figure import Figure

APPLICATION_NAME = 'Stress Simulator'
AUTO_RESET = True

# ZMQ interface
ZMQcontext = zmq.Context()
ZMQsocket = ZMQcontext.socket(zmq.REQ)
ZMQsocket.connect("tcp://localhost:5555")

class StressSimulator(QtWidgets.QMainWindow):
    # Override the class constructor
    def __init__(self, parent=None):
        super(StressSimulator, self).__init__(parent)
        # Setting main Widget
        self.main = QtWidgets.QWidget()
        self.setCentralWidget(self.main)

        # Setting title
        self.setWindowTitle(APPLICATION_NAME)

        # Setting geometry and minimum size
        self.setGeometry(100, 100, 600, 600)
        self.setMinimumSize(QtCore.QSize(400, 400))

        # A figure instance to plot on
        self.figure = Figure()

        # Canvas widget that displays the 'figure'
        self.canvas = FigureCanvasQTAgg(self.figure)

        # Navigation widget and Menu bar
        self.toolbar = NavigationToolbar2QT(self.canvas, self)
        bar = self.menuBar()
        mfile = bar.addMenu("&File")
        msimulation = bar.addMenu("&Simulation")
    
        open_action = QtWidgets.QAction("&Open...", self)
        open_action.setShortcut('Ctrl+O')
        open_action.triggered.connect(self.openImage)
        mfile.addAction(open_action)
        
        exit_action = QtWidgets.QAction("&Quit", self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.triggered.connect(self.closeEvent)
        mfile.addAction(exit_action)

        gen_json_action = QtWidgets.QAction("&Generate JSON", self)
        gen_json_action.triggered.connect(self.generateJSON)
        msimulation.addAction(gen_json_action)

        edit_action = QtWidgets.QAction("&Edit settings", self)
        edit_action.triggered.connect(self.editSettings)
        msimulation.addAction(edit_action)

        run_action = QtWidgets.QAction("&Run simulation step", self)
        run_action.setShortcut('Ctrl+R')
        run_action.triggered.connect(self.runSimulation)
        msimulation.addAction(run_action)

        reset_action = QtWidgets.QAction("&Reset simulation", self)
        reset_action.triggered.connect(self.resetSimulation)
        msimulation.addAction(reset_action)

        self.isStressGraphToggled = False
        self.currentDisplayedFigure = None
        toggleStressGraph_action = QtWidgets.QAction("&Toggle stress graph", self)
        toggleStressGraph_action.setShortcut('Ctrl+T')
        toggleStressGraph_action.triggered.connect(self.toggleStressGraph)
        msimulation.addAction(toggleStressGraph_action)

        exportSimulation_action = QtWidgets.QAction("&Export simulation", self)
        exportSimulation_action.setShortcut('Ctrl+S')
        exportSimulation_action.triggered.connect(self.exportSimulation)
        msimulation.addAction(exportSimulation_action)
        
        # For debugging purposes (should be commented)
        #mdebug = bar.addMenu("&Debug")
        #mdump_action = QtWidgets.QAction("&Dump failure matrix", self)
        #mdump_action.triggered.connect(self.dumpMatrix)
        #mdebug.addAction(mdump_action)
        
        #msavegraph_action = QtWidgets.QAction("&Save graph", self)
        #msavegraph_action.triggered.connect(self.saveGraph)
        #mdebug.addAction(msavegraph_action)
        #mdata_action.triggered.connect(self.printMdata)
        #mdebug.addAction(mdata_action)
        #loadrgb_action = QtWidgets.QAction("&Load rgb", self)
        #loadrgb_action.triggered.connect(self.loadRGB)
        #mdebug.addAction(loadrgb_action)

        # For displaying simulation information
        self.simulationInfo = QtWidgets.QLabel(self.main)

        # Set layouts
        mainLayout = QtWidgets.QVBoxLayout(self.main)
        mainLayout.addWidget(self.simulationInfo)
        mainLayout.addWidget(self.toolbar)
        mainLayout.addWidget(self.canvas, QtWidgets.QSizePolicy.MinimumExpanding)

        # Initialize the main image data
        self.m_data = None # numpy array
        self.m_failures = None # numpy array
        self.m_image = None # QImage object
        self.m_stressHistory = [] # list

        # Initialize simulation parameters
        self.m_fileTitle = "" # without the .extension
        self.m_inputPath = ""
        self.numberOfSimulations = 0
        self.deltaStrain = 0.000100
        self.currentStrain = 0.0000
        self.currentStress = 0.0000
        self.ultimate = 235.0
        self.failureColorRGB = (255, 0, 0) # red
        self.typeOfAnalysis = "VonMises" # ["Default", "VonMises"]
        self.updateSimulationInfo()

        if AUTO_RESET:
            self.resetSimulation()

    def dumpMatrix(self):
        print(self.m_failures)
        np.save("failures", self.m_failures)

    # @Slot()
    def openImage(self):
        options = QtWidgets.QFileDialog.Options()
        files, _ = QtWidgets.QFileDialog.getOpenFileNames(self, "Open Tomo", "", "Image Files (*.tif);;Image Files (*.tiff)", options=options)
        if files:
            filepath = files[0]
            self.loadImageData(filepath, True)
            self.m_fileTitle = getFileTitleFromPath(filepath)
            self.m_inputPath = os.path.dirname(filepath)
            self.m_failures = np.zeros(self.m_data.shape, dtype=bool)
            self.m_stressHistory = []
    
    # @Slot()
    def closeEvent(self, event):
        result = QtWidgets.QMessageBox.question(self, "Exit",
             "Are you sure you want to exit the program?",
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
            )
        if result == QtWidgets.QMessageBox.Yes:
            ZMQsocket.send_string("quit")
            QtWidgets.qApp.quit()
        #else:
        #    pass

    # @Slot()
    def generateJSON(self):
        if not self.isInputSelected():
            _ = QtWidgets.QMessageBox.warning(self, 
                "Error",
                "Please load an image before generating the JSON."
            )
        else:
            materials = {}
            mat_i, cmat_i = np.unique(self.m_data, return_counts=True)
            for i in range(len(mat_i)):
                if mat_i[i] in materials:
                    materials[mat_i[i]] += cmat_i[i]
                else:
                    materials[mat_i[i]] = cmat_i[i]

            materials = dict(sorted(materials.items(), key=lambda x: x[0]))
            dimensions = np.array([self.m_data.shape[1], self.m_data.shape[0]], dtype=int)
            vol = self.m_data.shape[1] * self.m_data.shape[0]
            mat = np.array(list(materials.keys()))
            cmat = np.array(list(materials.values()))
            mat = np.vstack((mat, np.zeros((mat.shape[0]),dtype=int))).T
            cmat = cmat*100.0/vol
            jdata = {}
            jdata["type_of_analysis"] = 0
            jdata["type_of_solver"] = 0
            jdata["type_of_rhs"] = 0
            jdata["voxel_size"] = 1.0
            jdata["solver_tolerance"] = 1.0e-6
            jdata["number_of_iterations"] = 1000
            jdata["image_dimensions"] = dimensions.tolist()
            jdata["refinement"] = 1
            jdata["number_of_materials"] = mat.shape[0]
            
            jdata["properties_of_materials"] = mat.tolist()
            for i in jdata["properties_of_materials"]:
                i.append(0)

            jdata["volume_fraction"] = list(np.around(cmat,2))
            jdata["data_type"] = "uint8"
            # Save image data in JSON format
            target_json = self.m_inputPath + "/" + self.m_fileTitle + ".json"
            with open(target_json, 'w') as file_json:
                json.dump(jdata, file_json, sort_keys=False, indent=4, separators=(',', ': '))
    
    # @Slot()
    def editSettings(self):
        deltaStrain, ok = QtWidgets.QInputDialog.getDouble(self, 
                            "Step size",
                            "Enter the step size (ΔE):", self.deltaStrain, 0.000001, 1, 6
                        )
        if ok:
            self.deltaStrain = deltaStrain
            
            ultimate, ok = QtWidgets.QInputDialog.getDouble(self, 
                            "Ultimate",
                            "Enter the ultimate:", self.ultimate, 0.001, 1000000, 3
                        )
            if ok:
                self.ultimate = ultimate

                typeOfAnalysis, ok = QtWidgets.QInputDialog.getItem(self, 
                            "Type of simulation",
                            "Choose the type of simulation:",
                            ["Default", "VonMises"], ["Default", "VonMises"].index(self.typeOfAnalysis), False
                        )
                if ok:
                    self.typeOfAnalysis = typeOfAnalysis
        self.updateSimulationInfo()

    # @Slot()
    def runSimulation(self):
        if not self.isInputSelected():
            _ = QtWidgets.QMessageBox.warning(self, 
                "Error",
                "Please load an image before running the simulation."
            )
        else:
            if self.numberOfSimulations == 0 and os.path.exists("temp/"):
                    _ = QtWidgets.QMessageBox.warning(self,
                        "Error",
                        "An existing '/temp' folder was found in your directory.\nPlease reset the simulation before continuing."
                    )
            else:
                if self.numberOfSimulations == 0:
                    simulation_deltaStrain = self.deltaStrain
                    self.generateRaw()
                    
                    cmd = self.m_fileTitle + " "\
                    + self.m_inputPath + "/ "\
                    + str(self.deltaStrain) + " "\
                    + str(self.ultimate) + " "\
                    + str(0) + " "\
                    + str(self.typeOfAnalysis)
                    
                    ZMQsocket.send_string(cmd)
                else:
                    simulation_deltaStrain = self.currentStrain + self.deltaStrain

                    cmd = self.m_fileTitle + " "\
                    + self.m_inputPath + "/ "\
                    + str(simulation_deltaStrain) + " "\
                    + str(self.ultimate) + " "\
                    + str(1) + " "\
                    + str(self.typeOfAnalysis)
                    
                    ZMQsocket.send_string(cmd)
                
                ok = ZMQsocket.recv()
                print("ok: ", ok)

                self.plotImageWithFailures()

                self.numberOfSimulations += 1
                self.currentStrain = simulation_deltaStrain
                self.currentStress = np.load("temp/currentStress.npy")
                self.m_stressHistory.append((self.currentStress, self.currentStrain))
                
                self.updateSimulationInfo()

    # @Slot()
    def resetSimulation(self):
        if not AUTO_RESET:
            result = QtWidgets.QMessageBox.question(self, 
                    "Reset simulation",
                    "Are you sure you want to reset the simulation?\nThis will delete all files in the generated '/temp' folder.",
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
                )
        
        if AUTO_RESET or result == QtWidgets.QMessageBox.Yes:
            if self.isInputSelected():
                self.loadImageData(self.m_inputPath + "/" + self.m_fileTitle + ".tif", True)

            if os.path.exists("temp/"):
                shutil.rmtree("temp")

            self.m_stressHistory = []
            self.numberOfSimulations = 0
            self.currentStrain = 0
            self.currentStress = 0
            self.updateSimulationInfo()
        #else:
        #    pass

    # method
    def loadImageData(self, _filepath, _updateWindow):
        self.m_image = QtGui.QImage(_filepath)
        # Conversions to deal with just 8 bits images:
        # Convert Mono format to Indexed8
        if self.m_image.depth() == 1:
            self.m_image = self.m_image.convertToFormat(QtGui.QImage.Format_Indexed8)
        # Convert Grayscale16 format to Grayscale8
        if not self.m_image.format() == QtGui.QImage.Format_Grayscale8:
            self.m_image = self.m_image.convertToFormat(QtGui.QImage.Format_Grayscale8)
        
        self.m_data = convertQImageToNumpy(self.m_image)

        if _updateWindow:
            self.plotImage()

    # method
    def plotImage(self):
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        img = ax.imshow(self.m_data, cmap="gray", vmin=0, vmax=255)
        self.figure.colorbar(img)
        ax.figure.canvas.draw()

    # method
    def plotImageWithFailures(self):
        self.m_failures = np.load("temp/failed.npy")
        #self.dumpMatrix()
        #self.m_failures = np.reshape(self.m_failures, (10, 20))
        #self.m_failures = np.transpose(self.m_failures)

        convertedNumpyScalars = convertNumpyScalarsToNumpyRGB(self.m_data)
        for i in range(np.shape(self.m_failures)[0]):
            for j in range(np.shape(self.m_failures)[1]):
                if self.m_failures[i][j]:
                    convertedNumpyScalars[i][j] = self.failureColorRGB
        
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        img = ax.imshow(convertedNumpyScalars, cmap="gray", vmin=0, vmax=255)
        self.figure.colorbar(img)
        ax.figure.canvas.draw()
        #ax.figure.savefig(f"{self.typeOfAnalysis}")
        

    def toggleStressGraph(self):
        if not self.isStressGraphToggled:
            self.figure.clear()

            x_axis = []
            y_axis = []
            for stress, strain in self.m_stressHistory:
                x_axis.append(strain)
                y_axis.append(stress)
            ax = self.figure.subplots()
            ax.plot(x_axis, y_axis, "")
            ax.figure.canvas.draw()
            #ax.figure.savefig(f"{self.typeOfAnalysis}")

            self.isStressGraphToggled = not self.isStressGraphToggled
        else:
            # NOTE(Gustavo): toggling graph without having loaded an image
            # raises an error when redrawing the canvas
            if self.numberOfSimulations == 0:
                self.plotImage()
            else:
                self.plotImageWithFailures()

            self.isStressGraphToggled = not self.isStressGraphToggled

    def exportSimulation(self):
        pass

    # method
    def generateRaw(self):
        filename = self.m_fileTitle + ".raw"
        materials = {}

        if not os.path.exists("temp/"):
            os.mkdir("temp")

        with open("temp/" + filename, "bw") as file_raw:
            mat_i, cmat_i = np.unique(self.m_data, return_counts=True)
            for i in range(len(mat_i)):
                if mat_i[i] in materials:
                    materials[mat_i[i]] += cmat_i[i]
                else:
                    materials[mat_i[i]] = cmat_i[i]
            
            # Save image data in binary format
            self.m_data.tofile(file_raw)
        
    # method
    def updateSimulationInfo(self):
        txt_numSim = f"Number of simulations = {self.numberOfSimulations}"
        txt_deltaE = f"Step size (Δε) = {self.deltaStrain:.6f}"
        txt_currentE = f"Current total steps (Δε) = {self.currentStrain:.6f}"
        txt_currentS = f"Current average stress (σₓ) = {self.currentStress:.6f}"
        txt_ultimate = f"Ultimate = {self.ultimate:.3f}"
        txt_typeSim = f"Type of analysis = {self.typeOfAnalysis}"
        self.simulationInfo.setText(f"{txt_numSim}\n{txt_deltaE}\n{txt_currentE}\n{txt_currentS}\n{txt_ultimate}\n{txt_typeSim}")
    
    # method
    def isInputSelected(self):
        return not (self.m_inputPath == "" and self.m_fileTitle == "")

# Considering just 8 bits images and converting to single depth:
def convertQImageToNumpy(_qimg):
    h = _qimg.height()
    w = _qimg.width()
    ow = _qimg.bytesPerLine() * 8 // _qimg.depth()
    d = 0
    if _qimg.format() in (QtGui.QImage.Format_ARGB32_Premultiplied,
                          QtGui.QImage.Format_ARGB32,
                          QtGui.QImage.Format_RGB32):
        d = 4 # formats: 6, 5, 4.
    elif _qimg.format() in (QtGui.QImage.Format_Indexed8,
                            QtGui.QImage.Format_Grayscale8):
        d = 1 # formats: 3, 24.
    else:
        raise ValueError(".ERROR: Unsupported QImage format!")
    buf = _qimg.bits().asstring(_qimg.byteCount())
    res = np.frombuffer(buf, "uint8")
    res = res.reshape((h,ow,d)).copy()
    if w != ow:
        res = res[:,:w]
    if d >= 3:
        res = res[:,:,0].copy()
    else:
        res = res[:,:,0]
    return res

def convertNumpyScalarsToNumpyRGB(_numpyMatrix):
    matrixShape = np.shape(_numpyMatrix)
    newMatrix = np.zeros(matrixShape + (3, ), np.uint8)
    
    for i in range(matrixShape[0]):
        for j in range(matrixShape[1]):
            newMatrix[i][j] = tuple([_numpyMatrix[i][j]]) * 3

    return newMatrix

def getFileTitleFromPath(filepath):
    return os.path.splitext(os.path.basename(filepath))[0]

def run():
    # To ensure that every time you call QSettings not enter the data of your application,
    # which will be the settings, you can set them globally for all applications
    QtCore.QCoreApplication.setApplicationName(APPLICATION_NAME)
    
    # create pyqt5 app
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")
    
    # create the instance of our Window
    mw = StressSimulator()
    
    # showing all the widgets
    mw.show()
    
    # start the app
    sys.exit(app.exec_())

if __name__ == "__main__":
    run()